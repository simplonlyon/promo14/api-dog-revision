import { Dog } from "../entity/Dog";
import { connection } from "./connection";



export class DogRepository {

    /**
     * Method that gets all dogs from database
     * @returns {Promise<Dog[]>} The dogs
     */
    static async findAll() {
        const [rows] = await connection.query('SELECT * FROM dog');

        return rows.map(row => new Dog(row['name'], row['breed'], row['picture'], Boolean(row['available']), row['id']));
    }
    /**
     * Method that search dogs in db
     * @param {string} term The search term
     * @returns Dogs which name and/or breed match the search term
     */
    static async search(term) {
        const [rows] = await connection.query('SELECT * FROM dog WHERE CONCAT(name,breed) LIKE ?', ['%'+term+'%'])
        return rows.map(row => new Dog(row['name'], row['breed'], row['picture'], Boolean(row['available']), row['id']));

    }
    /**
     * Method that persists a dog in the database
     * @param {Dog} dog The dog to persist
     */
    static async add(dog) {
        const [result] = await connection.query('INSERT INTO dog (name,breed,picture) VALUES (?,?,?)', [dog.name, dog.breed, dog.picture]);
        dog.id = result.insertId;
        
    }
}