import express from 'express';
import cors from 'cors';
import { dogController } from './controller/dog-controller';

export const server = express();

server.use(express.json());
server.use(cors());

server.use(express.static('public'));


server.use('/api/dog', dogController);