import { Router } from "express";
import { DogRepository } from "../repository/DogRepository";
import {Dog} from '../entity/Dog';
import { uploader } from "../uploader";

export const dogController = Router();



dogController.get('/', async (req, resp) => {
    try {
        let dogs;
        if(req.query.search) {
            dogs = await DogRepository.search(req.query.search);
        }  else {
            dogs= await DogRepository.findAll();
        }
        resp.json(dogs);
    }catch(error) {
        console.log(error);
        resp.status(500).json(error);
    }
});

dogController.post('/', uploader.single('picture'), async (req, resp) => {
    try {
        let newDog = new Dog();
        Object.assign(newDog, req.body); 
        newDog.picture = '/uploads/'+req.file.filename;
        /*comme son nom l'indique le Object.assign va assigner au premier paramètre les
        propriétés du second paramètre, en gros c'est comme si on faisait ça : */
        // newDog.name = req.body.name;
        // newDog.breed = req.body.breed;
        // newDog.picture = req.body.picture;
        
        await DogRepository.add(newDog);

        resp.status(201).json(newDog);
    }catch(error) {
        console.log(error);
        resp.status(500).json(error);
    }
})